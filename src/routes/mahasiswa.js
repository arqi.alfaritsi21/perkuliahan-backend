const router = require("express").Router();
const { getAll, getNilai, getNilaiById, getAverage, getAverageById } = require("../controller/mahasiswa");


router.get("/", getAll);
router.get("/nilai", getNilai);
router.get("/nilai/:id", getNilaiById);
router.get("/average/:id", getAverageById);
router.get("/average", getAverage);



module.exports = router;
