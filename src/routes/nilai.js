const router = require("express").Router();
const { create, update, deleteNilai } = require("../controller/nilai");

router.post("/", create);
router.put("/", update);
router.delete("/", deleteNilai);



module.exports = router;
