const route = require("express").Router();


const mahasiswa = require("./mahasiswa");
const nilai = require("./nilai");



route.use("/", mahasiswa);
route.use("/nilai", nilai);



module.exports = route;
