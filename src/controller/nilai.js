const helper = require("../helper");
const { create, update, deleteNilai, cekNilai } = require("../model/nilai");

module.exports = {
  create: async (request, response) => {
    const { id_mahasiswa, id_matkul, nilai, keterangan } = request.body;
    const cekNilaiMahasiswa = await cekNilai(id_mahasiswa, id_matkul);

    if (!cekNilaiMahasiswa.length < 1) {
      return helper.response(
        response,
        400,
        `Nilai untuk mahasiswa tersebut pada mata kuliah yang bersangkutan telah diinputkan sebelumnya`
      );
    }

    const setData = {
      id_mahasiswa,
      id_matkul,
      nilai,
      keterangan,
    };
    try {
      const result = await create(setData);
      return helper.response(response, 200, "Success", result);
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
  update: async (request, response) => {
    const { id_mahasiswa, id_matkul, nilai, keterangan } = request.body;

    const setData = {
      id_mahasiswa,
      id_matkul,
      nilai,
      keterangan,
    };
    try {
      const result = await update(setData, id_mahasiswa, id_matkul);
      return helper.response(response, 200, "Success", result);
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
  deleteNilai: async (request, response) => {
    const { id_mahasiswa, id_matkul } = request.body;

    try {
      const result = await deleteNilai(id_mahasiswa, id_matkul);
      return helper.response(response, 200, "Success", result);
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
};
