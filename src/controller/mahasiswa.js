const helper = require("../helper");
const { getAll, getNilai, getNilaiById, getAverageById, getAverage } = require("../model/mahasiswa");

module.exports = {
  getAll: async (request, response) => {
    try {
      const result = await getAll();
      return helper.response(response, 200, "Success", result);
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
  getNilai: async (request, response) => {
    try {
      const result = await getNilai();
      return helper.response(response, 200, "Success", result);
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
  getNilaiById: async (request, response) => {
    try {
      const { id } = request.params;
      const result = await getNilaiById(id);
      if (result.length > 0) {
        return helper.response(
          response,
          200,
          "Success",
          result
        );
      } else {
        return helper.response(
          response,
          404,
          "Nilai Mahasiswa Tidak Ditemukan"
        );
      }
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
  getAverageById: async (request, response) => {

    try {
      const { id } = request.params;
      const result = await getAverageById(id);

      if (result.length > 0) {
        return helper.response(
          response,
          200,
          "Success",
          result
        );
      } else {
        return helper.response(
          response,
          404,
          "Nilai Mahasiswa Tidak Ditemukan"
        );
      }
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
  getAverage: async (request, response) => {
    try {
      const result = await getAverage();
      return helper.response(response, 200, "Success", result);
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
};
