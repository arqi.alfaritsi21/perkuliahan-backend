const connection = require("../config/mysql");

module.exports = {
  create: (setData) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO data_nilai SET ?", setData, (error, result) => {
        if (!error) {
          const newResult = {
            ...setData,
          };
          resolve(newResult);
        } else {
          reject(new Error(error));
        }
      });
    });
  },
  cekNilai: (id_mahasiswa, id_matkul) => {
    return new Promise((resolve, reject) => {
      connection.query("SELECT id_mahasiswa, id_matkul from data_nilai WHERE id_mahasiswa = ? AND id_matkul = ?", [id_mahasiswa, id_matkul], (error, result) => {
        if (!error) {
          resolve(result);
        } else {
          reject(new Error(error));
        }
      });
    });
  },
  update: (setData, id_mahasiswa, id_matkul) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE data_nilai SET ? WHERE id_mahasiswa = ? AND id_matkul = ?", [setData, id_mahasiswa, id_matkul], (error, result) => {
        if (!error) {
          const newResult = {
            id_mahasiswa: id_mahasiswa,
            id_matkul: id_matkul,
            ...setData,
          };
          resolve(newResult);
        } else {
          reject(new Error(error));
        }
      });
    });
  },
  deleteNilai: (id_mahasiswa, id_matkul) => {
    return new Promise((resolve, reject) => {
      connection.query("DELETE FROM data_nilai WHERE id_mahasiswa = ? AND id_matkul = ?", [id_mahasiswa, id_matkul], (error, result) => {
        if (!error) {
          const newResult = {
            id_mahasiswa: id_mahasiswa,
            id_matkul: id_matkul,
          };
          resolve(newResult);
        } else {
          reject(new Error(error));
        }
      });
    });
  },
};
