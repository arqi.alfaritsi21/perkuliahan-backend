const connection = require("../config/mysql");

module.exports = {
  getAll: () => {
    return new Promise((resolve, reject) => {
      connection.query("SELECT * from mahasiswa", (error, result) => {
        if (!error) {
          resolve(result);
        } else {
          reject(new Error(error));
        }
      });
    });
  },
  getNilaiById: (id) => {
    return new Promise((resolve, reject) => {
      connection.query("SELECT mahasiswa.id, nama, nama_matkul, nilai FROM data_nilai LEFT JOIN mahasiswa ON mahasiswa.id = data_nilai.id_mahasiswa LEFT JOIN mata_kuliah ON data_nilai.id_matkul = mata_kuliah.id WHERE data_nilai.id_mahasiswa = ?", id, (error, result) => {
        if (!error) {
          resolve(result);
        } else {
          reject(new Error(error));
        }
      });
    });
  },
  getNilai: () => {
    return new Promise((resolve, reject) => {
      connection.query("SELECT mahasiswa.id, nama, nama_matkul, nilai FROM data_nilai LEFT JOIN mahasiswa ON mahasiswa.id = data_nilai.id_mahasiswa LEFT JOIN mata_kuliah ON data_nilai.id_matkul = mata_kuliah.id", (error, result) => {
        if (!error) {
          resolve(result);
        } else {
          reject(new Error(error));
        }
      });
    });
  },
  getAverageById: (id) => {
    return new Promise((resolve, reject) => {
      connection.query("SELECT nama, AVG(nilai) FROM data_nilai LEFT JOIN mahasiswa ON data_nilai.id_mahasiswa = mahasiswa.id WHERE id_mahasiswa = ? GROUP BY nama", id, (error, result) => {
        if (!error) {
          resolve(result);
        } else {
          reject(new Error(error));
        }
      });
    });
  },
  getAverage: () => {
    return new Promise((resolve, reject) => {
      connection.query("SELECT nama, AVG(nilai) FROM data_nilai LEFT JOIN mahasiswa ON data_nilai.id_mahasiswa = mahasiswa.id GROUP BY nama", (error, result) => {
        if (!error) {
          resolve(result);
        } else {
          reject(new Error(error));
        }
      });
    });
  },
};
